import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.xand0.R

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var button1: Button
    private lateinit var button2: Button
    private lateinit var button3: Button
    private lateinit var button4: Button
    private lateinit var button5: Button
    private lateinit var button6: Button
    private lateinit var button7: Button
    private lateinit var button8: Button
    private lateinit var button9: Button
    private lateinit var reset: Button
    private lateinit var player1: TextView
    private lateinit var player2: TextView

    private var firstPlayerPoint = 0
    private var secondPlayerPoint = 0


    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()

    private var activePlayer = 1

    private fun init() {
        button1 = findViewById<Button>(R.id.Button1)
        button2 = findViewById<Button>(R.id.Button2)
        button3 = findViewById<Button>(R.id.Button3)
        button4 = findViewById<Button>(R.id.Button4)
        button5 = findViewById<Button>(R.id.Button5)
        button6 = findViewById<Button>(R.id.Button6)
        button7 = findViewById<Button>(R.id.Button7)
        button8 = findViewById<Button>(R.id.Button8)
        button9 = findViewById<Button>(R.id.Button9)
        reset = findViewById<Button>(R.id.reset)


        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        reset.setOnClickListener {
            activePlayer = 1
            firstPlayer.clear()
            secondPlayer.clear()
            for (ButtonNumber in 1..9) {
                val clickedView: Button = when (ButtonNumber) {
                    1 -> button1
                    2 -> button2
                    3 -> button3
                    4 -> button4
                    5 -> button5
                    6 -> button6
                    7 -> button7
                    8 -> button8
                    9 -> button9
                    else -> {
                        button1
                    }
                }
                clickedView.text = ""
                clickedView.setBackgroundColor(Color.BLUE)
                clickedView.isEnabled = true

            }
        }

    }

    private fun halt() {

        button1.isEnabled = false
        button2.isEnabled = false
        button3.isEnabled = false
        button4.isEnabled = false
        button5.isEnabled = false
        button6.isEnabled = false
        button7.isEnabled = false
        button8.isEnabled = false
        button9.isEnabled = false

    }


    private fun point() {

        player1.text = "Player 1: $firstPlayerPoint"
        player2.text = "Player 2: $secondPlayerPoint"
    }


    override fun onClick(clickedView: View?) {
        if (clickedView is Button) {
            var ButtonNumber = 0
            when (clickedView.id) {
                R.id.Button1 -> ButtonNumber = 1
                R.id.Button2 -> ButtonNumber = 2
                R.id.Button3 -> ButtonNumber = 3
                R.id.Button4 -> ButtonNumber = 4
                R.id.Button5 -> ButtonNumber = 5
                R.id.Button6 -> ButtonNumber = 6
                R.id.Button7 -> ButtonNumber = 7
                R.id.Button8 -> ButtonNumber = 8
                R.id.Button9 -> ButtonNumber = 9

            }
            if (ButtonNumber != 0) {
                playGame(clickedView, ButtonNumber)
            }
        }

    }


    private fun playGame(clickedView: Button, ButtonNumber: Int) {
        if (activePlayer == 1) {
            clickedView.text = "x"
            clickedView.setBackgroundColor(Color.YELLOW)
            activePlayer = 2
            firstPlayer.add(ButtonNumber)
        } else {
            clickedView.text = "0"
            clickedView.setBackgroundColor(Color.GREEN)
            activePlayer = 1
            secondPlayer.add(ButtonNumber)
        }
        clickedView.isEnabled = false
        check()


    }

    private fun check() {
        var winnerPlayer = 0
        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(4) && secondPlayer.contains(5) && secondPlayer.contains(6)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(7) && secondPlayer.contains(8) && secondPlayer.contains(9)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(4) && secondPlayer.contains(7)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(2) && secondPlayer.contains(5) && secondPlayer.contains(8)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(6) && secondPlayer.contains(9)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(5) && secondPlayer.contains(9)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(5) && firstPlayer.contains(7)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(5) && secondPlayer.contains(7)) {
            winnerPlayer = 2
        }

        if (winnerPlayer == 1) {
            Toast.makeText(this, "X is winner", Toast.LENGTH_SHORT).show()
            firstPlayerPoint += 1
            point()
            halt()
        }

        if (winnerPlayer == 2) {
            Toast.makeText(this, "O is winner", Toast.LENGTH_SHORT).show()
            secondPlayerPoint += 1
            point()
            halt()

        }

    }

}


